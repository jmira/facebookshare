package com.nereasoft.facebookshare;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int ACTION_PICK_IMAGE = 1;

    //Facebook client
    private CallbackManager fb_callbackManager;
    private LoginManager fb_manager;

    private Button button;
    private ImageView imageView;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Facebook API
        FacebookSdk.sdkInitialize(getApplicationContext());
        fb_callbackManager = CallbackManager.Factory.create();
        fb_manager = LoginManager.getInstance();
        registerFacebookCallbaks();

        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editText);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView:
                loadImage();
                break;
            case R.id.button:
                share();
                break;
        }
    }

    private void loadImage(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra("crop", "true");
        photoPickerIntent.putExtra("scale", true);
        photoPickerIntent.putExtra("scaleUpIfNeeded", true);
        photoPickerIntent.putExtra("max-width", 170);
        photoPickerIntent.putExtra("max-height", 170);
        photoPickerIntent.putExtra("aspectX", 1);
        photoPickerIntent.putExtra("aspectY", 1);

        startActivityForResult(photoPickerIntent, ACTION_PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ACTION_PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                //get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                imageView.setImageBitmap(thePic);
            }
        }
        fb_callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    protected void share(){
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        fb_manager.logInWithPublishPermissions(this, permissionNeeds);
    }

    public void registerFacebookCallbaks(){
        fb_manager.registerCallback(fb_callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                doFacebookShare();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
            }
        });
    }

    private void doFacebookShare(){
        String shareText = editText.getText().toString();

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        ShareDialog shareDialog = new ShareDialog(this);
        if(ShareDialog.canShow(SharePhotoContent.class)) {
            shareDialog.registerCallback(fb_callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    Toast.makeText(MainActivity.this, "Share Success", Toast.LENGTH_SHORT).show();
                    Log.d("DEBUG", "SHARE SUCCESS");
                }

                @Override
                public void onCancel() {
                    Toast.makeText(MainActivity.this, "Share Cancelled", Toast.LENGTH_SHORT).show();
                    Log.d("DEBUG", "SHARE CACELLED");
                }

                @Override
                public void onError(FacebookException exception) {
                    Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    Log.e("DEBUG", "Share: " + exception.getMessage());
                    exception.printStackTrace();
                }
            });

            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(bitmap)
                    .setCaption(shareText)
                    .build();

            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();

            //ShareApi.share(content, null);
            Toast.makeText(this, shareText, Toast.LENGTH_SHORT);
            shareDialog.show(content);
        }
        else{
            Log.d("DEBUG", "SHARE DIALOG CAN NOT SHOW PHOTO CONTENT");
        }
    }

    private File saveTempBitmap(Bitmap bitmap){
        try {
            File outputDir = this.getExternalCacheDir(); // context being the Activity pointer
            File outputFile = File.createTempFile("img_", ".png", outputDir);
            FileOutputStream fOut = new FileOutputStream(outputFile);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            return outputFile;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
